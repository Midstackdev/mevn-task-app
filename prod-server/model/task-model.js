'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TaskSchema = new _mongoose2.default.Schema({
	title: {
		type: String
	},
	body: {
		type: String
	},
	dueDate: {
		type: Date,
		default: Date.now
	},
	completed: {
		type: Boolean,
		default: false
	},
	author: {
		type: _mongoose2.default.Schema.Types.ObjectId,
		ref: 'user'
	}
});
TaskSchema.set('timestamps', true);

exports.default = _mongoose2.default.model('tasks', TaskSchema);