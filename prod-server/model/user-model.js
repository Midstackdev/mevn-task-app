'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _stringUtil = require('../utilities/string-util');

var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var UserSchema = new _mongoose2.default.Schema({
	username: String,
	firstname: String,
	lastname: String,
	password: String
});

UserSchema.set('timestamps', true);
UserSchema.virtual('fullName').get(function () {
	var firstname = _stringUtil.StringUtil.capitalize(this.firstname.toLowerCase());
	var lastname = _stringUtil.StringUtil.capitalize(this.lastname.toLowerCase());
	return firstname + ' ' + lastname;
});

UserSchema.statics.passwordMatches = function (password, hash) {
	return _bcrypt2.default.compareSync(password, hash);
};

UserSchema.pre('save', function (next) {
	this.username = this.username.toLowerCase();
	this.firstname = this.firstname.toLowerCase();
	this.lastname = this.lastname.toLowerCase();
	var unsafePassword = this.password;
	var saltRounds = 5;
	this.password = _bcrypt2.default.hashSync(unsafePassword, saltRounds);
	next();
});

exports.default = _mongoose2.default.model('users', UserSchema);