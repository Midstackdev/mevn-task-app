import mongoose from 'mongoose'

const TaskSchema = new mongoose.Schema({
	title: {
		type: String
	},
	body: {
		type: String
	},
	dueDate: {
		type: Date, 
		default: Date.now
	},
	completed: {
		type: Boolean,
		default: false
	},
	author: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'user'
	}
})
TaskSchema.set('timestamps', true)

export default mongoose.model('tasks', TaskSchema)