import mongoose from 'mongoose'
import { StringUtil } from '../utilities/string-util'
import bcrypt from 'bcrypt'

const UserSchema = new mongoose.Schema({
	username: String,
	firstname: String,
	lastname: String,
	password: String
})

UserSchema.set('timestamps', true)
UserSchema.virtual('fullName').get(function() {
	const firstname = StringUtil.capitalize(this.firstname.toLowerCase())
	const lastname = StringUtil.capitalize(this.lastname.toLowerCase())
	return `${firstname} ${lastname}`
})

UserSchema.statics.passwordMatches = function(password, hash) {
	return bcrypt.compareSync(password, hash)
}

UserSchema.pre('save', function(next){
	this.username = this.username.toLowerCase()
	this.firstname = this.firstname.toLowerCase()
	this.lastname = this.lastname.toLowerCase()
	const unsafePassword = this.password
	const saltRounds = 5
	this.password = bcrypt.hashSync(unsafePassword, saltRounds)
	next()
})

export default mongoose.model('users', UserSchema)