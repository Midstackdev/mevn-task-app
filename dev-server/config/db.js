import mongoose from 'mongoose'

export function connectedToDB() {
	mongoose.connect(process.env.DB_URL, {useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false},
		error=> {
			if(error) {
				console.log('Unable to connect to dtabase')
				throw error
			}else{
				console.log('Connected to MongoDB!')
			}
		})
}