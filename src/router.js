import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/authentication/Login.vue'
import Register from './views/authentication/Register.vue'
import TaskAll from './views/tasks/TaskAll.vue'
import TaskCreate from './views/tasks/TaskCreate.vue'
import TaskEdit from './views/tasks/TaskEdit.vue'
import * as auth from './services/AuthService'

Vue.use(Router)

// const isLoggedIn = false

const routes = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/tasks',
      name: 'task-all',
      component: TaskAll,
      beforeEnter: (to, from, next) => {
        auth.isLoggedIn() ? next() : next('/login')
      }
    },
    {
      path: '/tasks/new',
      name: 'task-create',
      component: TaskCreate,
      beforeEnter: (to, from, next) => {
        auth.isLoggedIn() ? next() : next('/login')
      }
    },
    {
      path: '/tasks/:id',
      name: 'task-edit',
      component: TaskEdit,
      beforeEnter: (to, from, next) => {
        auth.isLoggedIn() ? next() : next('/login')
      }
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      beforeEnter: (to, from, next) => {
        !auth.isLoggedIn() ? next() : next('/')
      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      beforeEnter: (to, from, next) => {
        !auth.isLoggedIn() ? next() : next('/')
      }
    },
    {
      path: '*',
      redirect: '/'
    }
  ],
  linkActiveClass: 'active'
})

export default routes
